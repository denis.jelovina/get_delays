import sys
from  numpy import *
from numpy.linalg import norm
import numpy as np
import argparse
from scipy.special import sph_harm
from scipy.spatial.transform import Rotation as R
from scipy.linalg import det
from matplotlib import pyplot as plt
import copy



quadpylebdict=np.load("lebedevquad.npy",allow_pickle=True).item()
parser = argparse.ArgumentParser(description='Calcualte time delay.')

parser.add_argument('--ePolyScatFile', 
                    help='file name of ePolyScat output file. Can be used istead of split files. No --nEnergies and --nRecords are needed.')

parser.add_argument('--quadfile',
                    help='provide alternative file with quadrature points', default="lebedevquad.npy")

parser.add_argument('--quad_el', 
                    help='select quadrature for electron ejection integration:'+(' ,'.join(list(quadpylebdict.keys()))),default='lebedev_015')

parser.add_argument('--quad_lp',
                    help='select quadrature for laser direction integration:'+(' ,'.join(list(quadpylebdict.keys()))),default='lebedev_031')

parser.add_argument('--figsave',
                    help='save results as pdf file',default=False,type=bool)

parser.add_argument('--el_detect_angle',
                                        help='set angle between central detection direction and light polarization', default=None,type=float)

parser.add_argument('--theta_el_max', 
                    help='[0,pi] select max theta integration angle around --el_detect_angle, for electron ejection angle, i.e. max opening angle',default=3.14159265359,type=float)

parser.add_argument('--theta_el_min', 
                    help='[0,pi] select min theta integration angle around --el_detect_angle, for electron ejection angle, i.e. min opening angle',default=0,type=float)

parser.add_argument('--fileBase', 
                    help='base file name of ePolyScat files')

parser.add_argument('--nEnergies', 
                    help='numer of energies',type=int)

parser.add_argument('--nRecords', 
                    help='nRecords: total number of files (nSymm x nEnergies)',type=int)


parser.add_argument('--lp_fixed_angle', nargs=2, 
                    help='set fixed light polarization angles phi_fix [0,pi] and theta_fix [0,pi]. When specified,  integration over molecular orientations is not performed.',type=float)

# parser.add_argument('--save_theta_el_resolved_data', default=False, type=bool, 
#                     help='Save electron ejection angle resolved time delay. Can be used only with --lp_fixed_angle. Output file name save_theta_el_resolved_data.txt is rewritten wich each run.')

parser.add_argument('--split_theta_el_resolved_data_by_en', default=False, type=bool, 
                    help='Save electron ejection angle resolved time delay into separate file for each energy.')

parser.add_argument('--split_theta_el_resolved_data_by_en_plot', default=False, type=bool, 
                    help='Plot electron ejection angle resolved time delay into separate file for each energy.')

parser.add_argument('--theta_el_ngrid_size', nargs=2, default=(32,32), type=int, 
                    help='Size of angular grids for electron ejection angle plots.')



n_alpha=50

args = parser.parse_args()
argdict=vars(args)
print("Input parameters:",argdict)


if (argdict['quadfile'] is not None) :
    quadfile=argdict["quadfile"]
    quadpylebdict=np.load(quadfile,allow_pickle=True).item()


ePolyScatFile=argdict["ePolyScatFile"]
fileBase=argdict["fileBase"]
nEnergies=argdict["nEnergies"]
nRecords=argdict["nRecords"]
scheme_lp=copy.deepcopy(quadpylebdict[argdict["quad_lp"]])
scheme_e=copy.deepcopy(quadpylebdict[argdict["quad_el"]])

theta_el_max=argdict["theta_el_max"]
theta_el_min=argdict["theta_el_min"]
save_theta_el_resolved_data=False
if argdict['lp_fixed_angle'] is not None:
    phi_lp_fix,theta_lp_fix=argdict['lp_fixed_angle']
    print("Integration over molecular orientation not performed: using lp_fixed_angle!")
    scheme_lp["azimuthal_polar"]=np.array([[phi_lp_fix,theta_lp_fix]])
    scheme_lp["weights"]=np.array([1.])
    save_theta_el_resolved_data=argdict['save_theta_el_resolved_data']
    split_theta_el_resolved_data_by_en=argdict['split_theta_el_resolved_data_by_en']
    split_theta_el_resolved_data_by_en_plot=argdict['split_theta_el_resolved_data_by_en_plot']
    theta_el_ngrid_size=argdict['theta_el_ngrid_size']
else:
    phi_fix,theta_fix=None,None

theta_el_lfg=argdict['el_detect_angle']
phi_el_fl=0
if ((theta_el_lfg is not None) and (phi_el_fl is not None) ):
    el_det_vec=array([sin(theta_el_lfg)*cos(phi_el_fl), sin(theta_el_lfg)*sin(phi_el_fl), cos(theta_el_lfg)])
else:
    el_det_vec=None
    

def getElAngle(theta_lp,phi_lp,theta_e,phi_e):
    r1 = R.from_rotvec(theta_lp * np.array([0, 1, 0]))
    r2 = R.from_rotvec(phi_lp * np.array([0, 0, 1]))
    r12=r2*r1
    v=array([sin(theta_e)*cos(phi_e), sin(theta_e)*sin(phi_e), cos(theta_e)])
    v_out=r12.apply(v)
    phi_out=angle(v_out[0]+1.j*v_out[1])
    if(v_out[2]>1):
        v_out[2]=1
    elif(v_out[2]<-1):
        v_out[2]=-1
    theta_out=arccos(v_out[2])
    return(theta_out,phi_out)


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


class ePolyScatData:
    def __init__(self,file,lines=None):
        self.file=file
        self.read_ePolyScat_E3(lines=lines)

    def read_ePolyScat_E3(self,lines=None):
        if lines is None:
            f=open(self.file)
            lines=f.readlines()
            f.close()
            
                                                                                
        eKE=lines[0].split()[-2]
        symm=(lines[4].split()[2])[1:]
        lmax=int(lines[7].split()[2])
#         ifin=next(i for i,l in enumerate(lines) if l.count("+ Command")==1)-1 #end of record (start=line9)
#         ifin=8+2*(lmax+1)+1
        tmp=[(len(l.split())==7)&(all([isfloat(x) for x in l.split()])) for l in lines[9:]]

        ifin=0
        while( tmp[ifin+1] ):
            ifin=ifin+1
            if(ifin==len(tmp)-1):
                ifin=len(tmp)-1
                break

        rlIn=array(list(map(lambda x:x.split(),lines[9:9+ifin])))
        
        rawIdyHead=['Raw matrix elements, legnth (1) or velocity (2) form','m,l,mu,it,rawIdy']
        rlnlHead=['Raw matrix elements, legnth (1) or velocity (2) form, reformatted into (real) magnitude and phase',
                  'l,m,rlm,nlm,mu,it']
        
        index1=rlIn[:,3]=='1'
        index2=rlIn[:,3]=='2'

        rawIdy1=[rlIn[index1,0].astype(int), rlIn[index1,1].astype(int), rlIn[index1,2].astype(int), 
                 rlIn[index1,4].astype(int), rlIn[index1,5].astype(float)+rlIn[index1,6].astype(float)*1.j];
        rlnl1=[rawIdy1[1], rawIdy1[0], abs(rawIdy1[4]), angle(rawIdy1[4]), rawIdy1[2], rawIdy1[3]];


        rawIdy2=[rlIn[index2,0].astype(int), rlIn[index2,1].astype(int), rlIn[index2,2].astype(int), 
                 rlIn[index2,4].astype(int), rlIn[index2,5].astype(float)+rlIn[index2,6].astype(float)*1.j];
        rlnl2=[rawIdy2[1], rawIdy2[0], abs(rawIdy2[4]), angle(rawIdy2[4]), rawIdy2[2], rawIdy2[3]];

        self.eKE=eKE
        self.symm=symm
#         self.rlIn=rlIn
        self.rawIdyHead=rawIdyHead
        self.rlnlHead=rlnlHead
        self.rawIdy1=rawIdy1
        self.rlnl1=rlnl1
        self.rawIdy2=rawIdy2
        self.rlnl2=rlnl2




class rlAll_:
    def ePolyScatFileSplit(self):
        f=open(ePolyScatFile)
        lines=f.readlines()
        f.close()

        #array((content[where(["ScatEng" in line for line  in content])[0][0]]).split()[1:]).astype(float)
        #engline=lines[where(["ScatEng" == l[:7] for l in lines])[0][0]]
        engline=lines[where(["ScatEng" in l for l  in lines])[0][0]]
        
        self.nEnergies=len(engline.split()[1:])

        dump_start_line="DumpIdy - dump out phase corrected and properly normalized dynamical coefs\n"
        dlsindx=where([dump_start_line == l for l in lines])[0]

        tmp=[(len(l.split())==7)&(all([isfloat(x) for x in l.split()])) for l in lines]

        splitfiles=[]
        for j,i in enumerate(dlsindx[:]):
            if (j!=len(dlsindx)-1):
                inext=dlsindx[j+1]
            else:
                inext=-1

            k=len(where(tmp[i+12:inext])[0])
            splitfiles.append(lines[i+3:i+12+k])
        return (splitfiles)


    
    def __init__(self,N,fileBase,ePolyScatFile=None,Nstart=0,Nend=-1):
        if ePolyScatFile is None:
            self.N=N
            self.fileBase=fileBase
            self.allfiles=[fileBase+str(i)+".dat" for i in range(Nstart+1,N+1+(Nend+1))]
            self.data=[ePolyScatData(fileBase+str(i)+".dat") for i in range(Nstart+1,N+1+(Nend+1))]
        else:
            splitfiles=self.ePolyScatFileSplit()
            self.N=len(splitfiles)
            self.data=[ePolyScatData(ePolyScatFile,lines=lines) for i,lines in enumerate(splitfiles)]
            del(splitfiles)


        self.rawIdy1_itlist=unique(concatenate([array(r.rawIdy1[3]).transpose() for r in self.data]))
        self.rawIdy1_indx=unique(concatenate([array(r.rawIdy1[:4]).transpose() for r in self.data], axis=0),axis=0)
        self.rawIdy1_val=zeros((len(self.rawIdy1_indx),len(self.data))).astype(complex)
        for ien,ren in enumerate(self.data):
            indxsort1=array([where(all(self.rawIdy1_indx==t,axis=1))[0][0]
                             for t in array(ren.rawIdy1[:4]).transpose()])
            self.rawIdy1_val[indxsort1,ien]=ren.rawIdy1[4]

        self.rawIdy2_itlist=unique(concatenate([array(r.rawIdy2[3]).transpose() for r in self.data]))
        self.rawIdy2_indx=unique(concatenate([array(r.rawIdy2[:4]).transpose() for r in self.data], axis=0),axis=0)
        self.rawIdy2_val=zeros((len(self.rawIdy2_indx),len(self.data))).astype(complex)
        for ien,ren in enumerate(self.data):
            indxsort2=array([where(all(self.rawIdy2_indx==t,axis=1))[0][0]
                             for t in array(ren.rawIdy2[:4]).transpose()])
            self.rawIdy2_val[indxsort2,ien]=ren.rawIdy2[4]



def time_delay(theta_lp,phi_lp,theta_e,phi_e,rlAll,nsyms,symm,lmax=20):
    # angles in molecular frame
    nEnergies=len(rlAll.data)//nsyms
    if (nsyms*nEnergies != len(rlAll.data)):
        print("error: nsyms not divisible by num of data")
    s=set(array([(d.symm) for d in rlAll.data[0:nRecords//nsyms]]))
    if (len(s) != 1):
        print("error: not unique symmetry:",s)
    #index of energies corresponding to symm symmetry 
    enIndx=array(arange(nEnergies))+nEnergies*symm
    energies=array([float(rlAll.data[ierg].eKE) for ierg in enIndx])
    energies=energies/au2eV
    td_all=zeros((len(rlAll.rawIdy1_itlist),len(enIndx)))
    cs_all=zeros((len(rlAll.rawIdy1_itlist),len(enIndx)))
    # sum over degenerate states
    for jit,it in  enumerate(rlAll.rawIdy1_itlist):
        wf=zeros(nEnergies).astype(complex)
        cs=zeros(nEnergies)
        lmRange=(rlAll.rawIdy1_indx[:,1]<lmax)& (rlAll.rawIdy1_indx[:,3]==it)
        iarr=rlAll.rawIdy1_indx[lmRange,:3]
        varr=rlAll.rawIdy1_val[lmRange,:]
        for (m,l,mu),val in zip(iarr,varr):
            wf=wf+val[enIndx]*conjugate(sph_harm(m,l,phi_e,theta_e))*conjugate(sph_harm(mu,1,phi_lp,theta_lp))
            
        cs=abs(wf)**2
        
        td=gradient(unwrap(angle(wf)),energies) #* abs(wf)**2/cs
        td_all[jit,:]=td
        cs_all[jit,:]=cs
    if(norm(cs_all)==0.):
        td_deg_stat_avg=average(td_all, axis=0, weights=None)
    else:
        td_deg_stat_avg=average(td_all, axis=0, weights=cs_all)
    return(td_deg_stat_avg,sum(cs_all,axis=0))





def time_cs_tot(rlAll,nsyms,symm,lmax=20):
    # angles in molecular frame
    nEnergies=len(rlAll.data)//nsyms
    if (nsyms*nEnergies!=len(rlAll.data)):
        print("error: nsyms not divisible by num of data")
    enIndx=array(arange(nEnergies))+nEnergies*symm
    energies=array([float(rlAll.data[ierg].eKE) for ierg in enIndx])
    energies=energies/au2eV
    cs=zeros(nEnergies)
    for it in  rlAll.rawIdy1_itlist:
        lmRange=(rlAll.rawIdy1_indx[:,1]<lmax)& (rlAll.rawIdy1_indx[:,3]==it)
        iarr=rlAll.rawIdy1_indx[lmRange,:3]
        varr=rlAll.rawIdy1_val[lmRange,:]
        cs=cs+sum(abs(varr[:,enIndx])**2,axis=0)

    return(energies,cs)




phot_eV = 1.5498; # photon energy in eV
au2eV = 27.211;
planck_eVas = 4.135667662e-15*1e18; #eV*as
au2asec = 212.183;
gauge = 1;
# it=0;
au2as=1/0.041341374575751

readVec=nRecords
rlAll=rlAll_(readVec,fileBase,ePolyScatFile)
nRecords=len(rlAll.data)
if ePolyScatFile is not None:
    nEnergies=rlAll.nEnergies


nsyms=nRecords//nEnergies

if(nsyms*nEnergies != nRecords):
    print("---> Error: number of records is wrong  ")
    sys.exit()

nEnergiesQ=len(rlAll.data)//nsyms
if(nEnergiesQ != nEnergies):
    print("error: nEnergies doesnt match")
    sys.exit()

engall=array([float(d.eKE) for d in rlAll.data])
tmp1=array_split(engall, nsyms)
for i in range(1,len(tmp1)):
    if(norm(tmp1[0]-tmp1[i]) > 1.e-5 ):
        print ("error: nenergies do not match")
        sys.exit()
energies=array([float(d.eKE) for d in rlAll.data[0:nRecords//nsyms]])





# #el ejection angle resolved data 
# if (save_theta_el_resolved_data):
#     print ("performing angular resolved calcualtion")
#     phi_lp,theta_lp=phi_lp_fix,theta_lp_fix
#     n,m=theta_el_ngrid_size
#     phi_e_arr=linspace(0,2*pi,n)
#     theta_e_arr=linspace(0,pi,m)

#     for m in range(nsyms):
#         k=0        
#         tmparr=zeros((len(phi_e_arr)*len(theta_e_arr),len(energies)+2))
#         for ii,phi_e in enumerate(phi_e_arr):
#             for jj,theta_e in enumerate(theta_e_arr):
#                 td=time_delay(theta_lp,phi_lp,theta_e,phi_e,rlAll,nsyms,m)
#                 tmparr[k,0]=phi_e
#                 tmparr[k,1]=theta_e
#                 tmparr[k,2:]=-td*au2as
#                 k=k+1
#         s=set(array([(d.symm) for d in rlAll.data[nRecords//nsyms*m:nRecords//nsyms*(m+1)]]))
#         header="# phi_el                  theta_el                "+"".join(["tau("+str(eng)+")         " for eng in energies])
#         fanme="save_theta_el_resolved_data_symm"+str(m)+"_"+list(s)[0]+".txt"
#         np.savetxt(fanme,tmparr,header=header)
#         if ( split_theta_el_resolved_data_by_en ):
#             for ien,en in enumerate(energies):
#                 header="# phi_el                  theta_el                "+"tau("+str(energies[ien])+")"
#                 fanme="save_theta_el_resolved_data_symm"+(str)+"_"+list(s)[0]+"_en"+str(ien)+".txt"
#                 np.savetxt(fanme,tmparr[:,[0,1,2+ien]],header=header)
#                 if(split_theta_el_resolved_data_by_en_plot):
#                     fig = plt.figure(figsize=(10,8))
#                     ax = plt.gca()
#                     X,Y=meshgrid(theta_e_arr,phi_e_arr)
#                     Z=reshape(tmparr[:,2+ien],shape(X))
#                     fanme="save_theta_el_resolved_data_symm_"+list(s)[0]+"_en"+str(ien)+".pdf"
#                     p=plt.pcolormesh(X,Y,Z,shading='auto')
#                     ax.set_xlabel(r"$\theta_e$ / rad")
#                     ax.set_ylabel(r"$\phi_e$ / rad")
#                     plt.colorbar(p,ax=ax)
#                     plt.savefig(fanme, bbox_inches = "tight")
#     sys.exit()

    



    





#el ejection angle integrated data 
tdcsdata=[]
print("nsyms=",nsyms)
for m in range(nsyms):
    tdarr_en_lp_e=zeros((len(energies),len(scheme_lp["azimuthal_polar"]),len(scheme_e["azimuthal_polar"])))
    csarr_en_lp_e=zeros((len(energies),len(scheme_lp["azimuthal_polar"]),len(scheme_e["azimuthal_polar"])))
    for jj,(phi_lp,theta_lp) in enumerate(scheme_lp["azimuthal_polar"]):

        if (el_det_vec is not None):
            # vdet_lab=copy.deepcopy(el_det_vec)
            # vdet_lab=vdet_lab/norm(vdet_lab)
            rx=R.from_euler('yz', [theta_lp,phi_lp], degrees=False)
            vec_lp=array([sin(theta_lp)*cos(phi_lp),sin(theta_lp)*sin(phi_lp),cos(theta_lp)]) 
            # #**test**
            #v_lab=np.random.rand(3)
            ####v_mol = sum((rx.as_dcm())*v_lab,axis=1)  # molecular to lab frame transformation (same mas next line)
            #v_mol = rx.apply(v_lab)  # molecular to lab frame transformation
            #print("->ncheck=",norm(rx.inv().apply(v_mol)-v_lab))

            #v_lab=array([0,0,1])
            #v_mol = rx.apply(v_lab)  # molecular to lab frame transformation
            #print("->ncheck=",norm(v_mol-vec_lp))
        
        for ii,(phi_e,theta_e) in enumerate(scheme_e["azimuthal_polar"]):
            vec_e=array([sin(theta_e)*cos(phi_e), sin(theta_e)*sin(phi_e), cos(theta_e)]) #ejection vec in mol frame

            if (el_det_vec is not None):
                #vec_e_lab_it = sum((rx.as_dcm())*vec_e,axis=1) # intermediate lab frame
                vec_e_lab=rx.inv().apply(vec_e)
                theta_e_lab=arccos(vec_e_lab[2]/norm(vec_e_lab)) # need just theta angle 
                if(abs(theta_el_lfg-theta_e_lab)>theta_el_max):
                    # check if ejection direction is within acceptance angle, skip if it is
                    continue

                if(abs(theta_el_lfg-theta_e_lab)<theta_el_min):
                    # check if ejection direction is within acceptance angle, skip if it is
                    continue


            td,cs=time_delay(theta_lp,phi_lp,theta_e,phi_e,rlAll,nsyms,m)
            tdarr_en_lp_e[:,jj,ii]=td
            csarr_en_lp_e[:,jj,ii]=cs



    X,Y=meshgrid(scheme_e["weights"],scheme_lp["weights"])
    td_symm=array([sum(csarr_en_lp_e[i,:,:]*tdarr_en_lp_e[i,:,:]*Y*X) for i,en in enumerate(energies)])
    cs_symm=array([sum(csarr_en_lp_e[i,:,:]*Y*X) for i,en in enumerate(energies)])

    en,cs=time_cs_tot(rlAll,nsyms,m,lmax=20)
    s=set(array([(d.symm) for d in rlAll.data[nRecords//nsyms*m:nRecords//nsyms*(m+1)]]))

    print("symm=",s)
    print("energies=",energies)
    print("delay=",-td_symm/cs_symm)
    print("cs_partial=",cs_symm*(4*pi)**2)
    print("cs_tot=",cs)
    print("")



    


if(argdict["figsave"]):
    print("figsave  currently disabled")

    # import os
    # cwd = os.getcwd()
    # mainlabel=cwd.split("/")[-1]
    # mainsublabel=cwd.split("/")[-2]
    
    # fig = plt.figure(figsize=(10,8))
    # ax1 = plt.subplot2grid((2,2), (0,0))
    # ax2 = plt.subplot2grid((2,2), (0,1))
    # ax3 = plt.subplot2grid((2,2), (1,0), colspan=2)
    # for m in range(nsyms):
    #     s=set(array([(d.symm) for d in rlAll.data[nRecords//nsyms*m:nRecords//nsyms*(m+1)]]))
    #     td,cs=tdcsdata[m]
    #     ax1.plot(energies,td,label=s+" (symm"+str(m)+")")
    #     ax2.plot(energies,cs,label=s+" (symm"+str(m)+")")
    
    #     td_symm_avg=sum(array([cs*td for td,cs in tdcsdata]),axis=0)/sum(array([cs for td,cs in tdcsdata]),axis=0)
    #     ax3.plot(energies,td_symm_avg*au2as)

    # ax1.legend()
    # ax2.legend()
    # ax1.set_xlabel("energy / eV")
    # ax1.set_ylabel("delay / as")
    # ax2.set_xlabel("energy / eV")
    # ax2.set_ylabel(r"$\sigma$ / Mb")
    # ax3.set_xlabel("energy / eV")
    # ax3.set_ylabel("delay (cs. wgh. avg.) / Mb")
    # plt.savefig('time_delay_'+mainlabel+'_'+mainsublabel+'.pdf', bbox_inches = "tight")
